# Otus kubernetes couse project

Итоговая работа по курсу "Инфраструктурная платформа на основе Kubernetes"

# Кластер

Для выполнения курсовой работы развернут кластер в Yandex Cloud.

## Установка

Для кластера создан отдельный каталог kuber-otus. Кластер развернут при помощи инструкции https://cloud.yandex.ru/docs/managed-kubernetes/quickstart 
в веб-интерфейсе консоли YC.

Так как развернуть кластер, используя CLI тулзу yc не представляется возможным, задание по автоматическому развертыванию 
кластера не выполнено.

В GCP gloud позволяет такое делать, AWS eksctl тоже позволяет применить YML файл. С инструментами Ansible/Terraform 
я не знаком и в рамках данного курса они не освещались. Если останется время, изучу, попробую сделать.

![alt text](img/img.png)

### Конфигурация кластера:

![alt text](img/img1.png)

### Node groups:

Кластер содержит три группы узлов для разных задач:

![alt text](img/img_1.png)

cluster-pool - общие для кластера сервисы: cert-manager, Istio, Flagger
monitoring-pool - сервисы для мониторинга и логирования кластера: Prometheus, Loki, Grafana, Jaeger
application-pool - развертываемое приложение

Важно при создании пулов выставить нодам соответствующие метки (https://cloud.yandex.ru/docs/managed-kubernetes/concepts/#node-labels), 
по которым в будущем будут шедулиться поды. Таковой меткой стал ключ `assignment` со значением cluster/monitoring/application 
для соответствующих пулов. Затем, при создании спецификаций подов, можно выставить параметр:

    spec:
        nodeSelector:
            assignment: cluster

под развернется в пуле cluster-pool.

### Конфигурация yc cli

Установлена утилита управления yandex cloud https://cloud.yandex.ru/docs/cli/quickstart

    Get-ExecutionPolicy
    Restricted 
    Set-ExecutionPolicy unrestricted
    Get-ExecutionPolicy
    Unrestricted
    iex .\install.ps1
    Downloading yc 0.74.0
    Yandex.Cloud CLI 0.74.0 windows/amd64
    Now we have zsh completion. Type "echo 'source C:\Users\alexp\yandex-cloud\completion.zsh.inc' >>  ~/.zshrc" to install itAdd yc installation dir to your PATH? [Y/n]: y

С помощью `yc init` сконфигурирован доступ к кластеру

    yc managed-kubernetes cluster get-credentials otus --external
    
    Context 'yc-otus' was added as default to kubeconfig 'C:\Users\alexp\.kube\config'.
    Check connection to cluster using 'kubectl cluster-info --kubeconfig C:\Users\alexp\.kube\config'.
    
    Note, that authentication depends on 'yc' and its config profile 'default'.
    To access clusters using the Kubernetes API, please use Kubernetes Service Account.

Проверка: 

    kubectl config view
    apiVersion: v1
    clusters:
    - cluster:
      certificate-authority-data: DATA+OMITTED
      server: https://84...
      name: yc-managed-k8s-...
    
    kubectl get nodes
    NAME                        STATUS   ROLES    AGE     VERSION
    cl1g4rhnq2a9fu4qk3ot-elyh   Ready    <none>   42m     v1.20.2
    cl1om7cja95q52f9bund-acax   Ready    <none>   3h46m   v1.20.2
    cl1om7cja95q52f9bund-oguz   Ready    <none>   3h46m   v1.20.2
    cl1ui0po0g4tmq8m94cb-iroz   Ready    <none>   4h      v1.20.2

Назначение группам узлов своих меток assignment:

    yc managed-kubernetes node-group add-labels cluster-services --labels assignment=cluster
    id: cat3...
    cluster_id: cat...
    created_at: "2021-04-04T12:18:29Z"
    name: cluster-services
    description: общие сервисы для кластера
    labels:
        assignment: cluster
    ...
    yc managed-kubernetes node-group add-labels monitoring-pool --labels assignment=monitoring
    ...
    yc managed-kubernetes node-group add-labels application-pool --labels assignment=application
    ...

Конфигурация вычислительных ресурсов кластера завершена

## Манифесты

После завершения работы над конфигурацией кластера все последующие работы с ним ведутся с помощью kubectl и yaml манифестов

Для проверки работы node labels создан манифест nginx-example.yml. Он будет установлен с помощью GitLab CI/CD pipeline.

### Настройка GitLab CI/CD

Чтобы подключить кластер kubernetes к GitLab, необходимо в репозитории проекта нажать на кнопку "Add Kubernetes cluster",
заполнить необходимые поля в открывшейся странице, руководствуясь инструкцией https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster

![alt text](img/img_2.png)

Для получения Service Token необходимо применить манифест gitlab/gitlab-admin-service-account.yaml, далее руководствоваться инструкцией 
по ссылке выше.

    kubectl apply -f gitlab-admin-service-account.yaml
    serviceaccount/gitlab created
    clusterrolebinding.rbac.authorization.k8s.io/gitlab-admin created
    
    kubectl -n kube-system get secret
    kubectl -n kube-system describe secret gitlab-token-....
    token:      eyJhbGciOi...

Интеграция прошла успешна. Для удобства работы ci установлен собственный gitlab runner через веб-интерфейс GitLab.

![alt text](img/img_3.png)
![alt text](img/img_4.png)
![alt text](img/img_5.png)

В .gitlab-ci.yml выставлены теги cluster
    
    namespaces:
        stage: namespaces
        tags:
          - cluster

Настройка pipeline произведена по инструкции https://cloud.yandex.ru/docs/solutions/infrastructure-management/gitlab-containers.

### Nginx Ingress и Cert-manager

В кластер установлен Ingress controller и Cert-manager с помощью helmfile и расположенных в текущем репозитории values.yml.

В панели управления доменными именами у регистратора добавлена А-запись, указывающая на external ip nginx ingress.  
Выдача сертификатов проверена на chartmuseum - он также добавлен в helmfile и использует values.yml.

![alt text](img/img_6.png)

### Loki-stack (Grafana, Loki, Prometheus, Promtail)

В качестве системы мониторинга и хранения логов используется Loki Stack (https://grafana.com/docs/loki/v1.6.1/installation/helm/).
Устанавливается через helmfile, в values.yml на месте пароля к Grafana admin установлена переменная окружения из GitLab
репозитория. Переменные окружения инжектятся утилитой envsubst в before_script:

    before_script:
    ...
      # install env vars
      - apk add gettext
      - envsubst < ./cluster-services/prometheus/values.yml > ./cluster-services/prometheus/values-ench.yml

Получившийся файл используется в helmfile.

Все компонентны Loki Stack устанавливаются в monitoring-pool, кроме prometheus-node-exporter и Promtail - они 
устанавливаются на каждую ноду для экспорта метрик и логов приложений в Loki.

![alt text](img/img_7.png)
![alt text](img/img_8.png)
![alt text](img/img_9.png)

Grafana устанавливается с несколькими предустановленными дэшбордами, которые берут метрики из Prometheus:

![alt text](img/img_10.png)
![alt text](img/img_11.png)

Для графаны настроен SMTP и для отправки алертов. Т.к. используется gmail, заранее получен пароль для приложения (см.
https://support.google.com/accounts/answer/185833?p=InvalidSecondFactor&visit_id=637538573067045682-228075235&rd=1)
Лучший способ создать нотификатор - сделать его руками в самой графане, затем обратиться 
    
    GET grafana.host/api/alert-notifications 

Получить JSON и применить его в yaml.

Для примера установлен dashboard с настроенными алертами (не все применились в Yandex cloud):

![alt text](img/img_12.png)

Promtail выгружает данные в Loki:

![alt text](img/img_13.png)

При желании есть возможность сделать кастомный Grafana Dashboard и подгрузить к остальным дэшбордам графаны.

### Istio

CI дополнен установкой Istio согласно инструкции https://istio.io/latest/docs/setup/getting-started/. Версия 1.9.2, 
профиль demo, оба параметра задаются в настройках Gitlab CI. Создается namespace application, ему выставляется label, 
чтобы при развертывании пода в данном неймспейсе, ему инжектился sidecar-контейнер Envoy. Т.к. в helmfile указана установка
Jaeger, необходимо узнать адрес jaeger-collector, чтобы istio направлял трафик коллектору на порт 9411. Как открыть порт,
описано ниже в разделе с Jaeger.

    - export JAEGER_COLLECTOR_IP=$(kubectl get service jaeger-collector -n jaeger -o jsonpath='{.spec.clusterIP}')
    # Istio
    - curl -L https://istio.io/downloadIstio | ISTIO_VERSION=$ISTIO_VERSION TARGET_ARCH=x86_64 sh -
    - cd istio-$ISTIO_VERSION
    - export PATH=$PWD/bin:$PATH
    - istioctl install --set profile=$ISTIO_PROFILE -y -f ./cluster-services/istio/istio-operator.yml
      --set values.global.tracer.zipkin.address=$JAEGER_COLLECTOR_IP:9411
    - set +e # Disable exit on error
    - kubectl create ns application
    - kubectl label namespace application istio-injection=enabled
    - set -e # Enable exit on error

Команда `istioctl install` конфигурирована параметром `-f ./cluster-services/istio/istio-operator.yml` для установки 
подов Istio на cluster-pool.

Согласно инструкции, https://istio.io/latest/docs/tasks/observability/distributed-tracing/configurability/#customizing-trace-sampling
значение параметра Trace sampling задается в values.yml:

    apiVersion: install.istio.io/v1alpha1
    kind: IstioOperator
    spec:
      meshConfig:
      enableTracing: true
      defaultConfig:
        tracing:
          sampling: 50

100% трейсов не нужно высылать в Jaeger. Я пока остановился на 50%.

![alt text](img/img_14.png)

Полезная статья: https://habr.com/ru/company/flant/blog/438426/

### Jaeger 

В helmfile добавлен Jaeger. 
В values.yml важно раскомментировать порт зипкина (читай выше в Istio --set values...zipkin.address=...)

    zipkin:
       port: 9411

![alt text](img/img_18.png)

### Kiali

Для графического представления связи сервисов установлен Kiali 
(https://kiali.io/documentation/latest/quick-start/#_install_via_kiali_server_helm_chart).
Доступ к дэшборду осуществляются посредством команды  

    istioctl dashboard kiali  

Для логина требуется токен. См. https://kiali.io/documentation/latest/faq/#:~:text=You%20can%20extract%20a%20service,you%20created%20the%20service%20account.&text=Note%20that%20this%20example%20assumes,token%20into%20the%20token%20field.

Kiali интегрируется с Prometheus, Grafana, Jaeger. На данный момент мной не решена проблема интеграции с Grafana и Jaeger, 
ввиду чего граф сервисов отображается без связей. Надеюсь, поправлю это в будушем, т.к. наблюдаемость с Kiali какая-то 
просто невероятная.

Ссылки для будущего дебага: https://istio.io/latest/docs/tasks/observability/kiali/, https://istio.io/latest/docs/ops/integrations/grafana/,
https://kiali.io/documentation/latest/faq/#could-not-fetch-traces

### GitOps с ArgoCD

Подробно об ArgoCD описано в статье https://habr.com/ru/company/otus/blog/544370/

Get started (https://argoproj.github.io/argo-cd/getting_started/) описывает установку с помощью kubectl apply manifest,
что не очень интересно. Имеется проект https://github.com/argoproj/argo-helm/tree/master/charts/argo-cd, он и использован
в helmfile.

Нормально поставить ArgoCD из helm-чарта не удалось. Столкнулся со следующими проблемами: 
- ни с какой конфигурацией не работает ingress
- не удалось залогиниться ни под паролем-именем пода, ни под установленным паролем в values. Последний вариант вообще 
можно просмотреть в секрете: `kubectl get secret argocd-secret -n argocd -o json | jq -r '.data."admin.password"' | base64 -d -`,
  и он, блин, правильный, но не заходит.
  Если удалить пароль, то прогрузится интерфейс и будет возвращать `Unable to load data: server.secretkey is missing`.
  Если бы поднимал Flux изначально, то уже давно бы все запустил, и не изучал заново инструмент.
  Но наработки оставлю в этом репо.
  
### GitOps с Flux

Для реализации практик DevOps в итоге выбран инструмент Flux, т.к. он понятный и прост в использовании.
Описаны отдельные stages: application и repo_sync. 

В первом развертываются flux и flux-operator из helmfile, создается секрет по инструкции https://docs.fluxcd.io/en/1.18.0/guides/provide-own-ssh-key.html. 
Секрет рекомендуется генерить на локальной машине, но т.к. здесь же хранится пайплайн, приватный ключ сохранен как файл 
в переменных репозитория. Публичный ключ с правами на запись находится в репозитории развертываемого приложения 
"Microservices-demo" - https://gitlab.com/AlexPit/microservicesdemo.
Важным моментом является то, что имя git.secretName: "flux-ssh" и git.ssh.secretName: flux-ssh должны совпадать, иначе 
будут ошибки деплоя приложения.

Так как возникли большие проблемы с установкой fluxctl в используемом образе на основе Alpine (или Arch) Linux (необходимо
устанавливать из snap, которого в образе нет, apk с этой задачей не справился. Можно с помощью apk установить pacman, чтобы тот поставил
makepkg, которым нужно собрать пакет https://snapcraft.io/install/fluxctl/arch, но возникли проблемы с созданием и 
переключением на non-root пользователя, т.к. makepkg не дает запуститься под рутом), сделал отдельный stage с образом, 
в котором есть и kubectl, и fluxctl. В stage repo_sync происходит синхронизация кластера с репозиторием приложения.

![alt text](img/img_15.png)
![alt text](img/img_16.png)

После синхронизации helmrelease установил указанные в репозитории приложения. Frontend решил сделать на развернувшемся 
Istio ingress, чтобы мониторить трафик и показать использование двух разных ингрессов. Например, через my-budget.ru
(домен, который был в распоряжении, настроен на external ip Nginx) и поддомены доступны различные инфраструктурные утилиты,
а через Istio ingress с ext ip 130.193.56.191 доступен фронтенд целевого приложения (второго домена не нашлось, а перебивать
айпишники в консоли управления доменом нет никакого желания).

Приложения установились с ошибками:

![alt text](img/img_22.png)
![alt text](img/img_20.png)

что сразу стало видно в Jaeger и Kiali:

![alt text](img/img_17.png)
![alt text](img/img_23.png)
![alt text](img/img_18.png)
![alt text](img/img_21.png)

Логи падающих подов можно посмотреть в Grafana:

![alt text](img/img_19.png)

В целом задача курсовой работы достигнута, занимаюсь отладкой. 
В идеале еще задачи:
- [ ] отладить демо приложение
- [ ] обеспечить его ssl сертификатом (https://istio.io/latest/docs/ops/integrations/certmanager/)
- [ ] отладить kiali+jaeger для построения графа трейсов
- [ ] для наглядности добавить схемы
- [ ] описать nodeSelector в демо приложении, чтобы разворачивалось только на необходимом пуле
- [ ] изучить ansible и/или terraform и описать stage создания кластера make-cluster
- [ ] после выполнения домашки по Vault использовать его для хранения секретов (и для обеспечения шифрования межсервисного взаимодействия)
- [ ] вынести из директории cluster-services директории, которые должны находиться в monitoring (jaeger, kiali, loki-stack)
